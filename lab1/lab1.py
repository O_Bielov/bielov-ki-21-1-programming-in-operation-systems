import os
import sys

def getFilesFromDirectory(path) -> list:
    files = os.listdir(path)
    result = []
    for file in files:
        if os.path.isfile(os.path.join(path, file)):
            result.append(file)
        else:
            result.append((file, getFilesFromDirectory(os.path.join(path, file))))
    return result

def outputElements(elements: list, tab: int = 0):
    tabOffset = "\t"*tab
    for element in elements:
        if isinstance(element, tuple):
            print(tabOffset + element[0])
            outputElements(element[1], tab + 1)
        else:
            print(tabOffset + element)

def main():
    dirPath = './'
    if len(sys.argv) > 1:
        dirPath = sys.argv[1]
    outputElements(getFilesFromDirectory(dirPath))

main()